﻿using Foundation;
using RealEstateMobile.Consts;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;


[assembly: Dependency(typeof(RealEstateMobile.iOS.Dependencies.Auth))]
namespace RealEstateMobile.iOS.Dependencies
{
    public class Auth : IAuth
    {
        public Auth()
        {

        }

        public string GetCurrentUserId()
        {
            return Firebase.Auth.Auth.DefaultInstance.CurrentUser.Uid;
        }

        public bool IsAuthenticated()
        {
            return Firebase.Auth.Auth.DefaultInstance.CurrentUser != null;
        }

        public async Task<bool> LoginUser(string email, string password)
        {
            try
            {
                await Firebase.Auth.Auth.DefaultInstance.SignInWithPasswordAsync(email, password);
                return true;
            }
            catch(NSErrorException err)
            {
                string msg = err.Message.Substring(err.Message.IndexOf("NSLocalizedDescription=", StringComparison.CurrentCulture));
                msg = msg.Replace("NSLocalizedDescription=", "").Split('.')[0];
                throw new Exception(msg);
            }
            catch(Exception)
            {
                throw new Exception("There was an unknown error!");
            }
        }

        public Task<bool> SignOut()
        {
            NSError error;
            bool var = Firebase.Auth.Auth.DefaultInstance.SignOut(out error);
            return Task.FromResult(var);
        }

        public async Task<string> RegisterUser(string email, string password)
        {
            try
            {
                var user = await Firebase.Auth.Auth.DefaultInstance.CreateUserAsync(email, password);
                string uid = user.User.Uid;
                return uid;
            }
            catch (NSErrorException err)
            {

                throw new Exception(err.Message);
            }
            catch (Exception)
            {
                throw new Exception("There wss an unknown error!");
            }
        }

        public string GetCurrentUserEmail()
        {
            return Firebase.Auth.Auth.DefaultInstance.CurrentUser.Email;
        }
    }
}