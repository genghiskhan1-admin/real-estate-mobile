﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Foundation;
using RealEstateMobile.Consts;
using RealEstateMobile.Models;
using RealEstateMobile.Views;
using Xamarin.Forms;

[assembly: Dependency(typeof(RealEstateMobile.iOS.Dependencies.Firestore))]
namespace RealEstateMobile.iOS.Dependencies
{
    public class Firestore : IFirestore
    {

        public Firestore()
        {
            
        }

        public async Task<List<SavedListing>> Read()
        {
            try
            {
                var collection = Firebase.CloudFirestore.Firestore.SharedInstance.GetCollection("savedListings");
                var query = collection.WhereEqualsTo("userId", Firebase.Auth.Auth.DefaultInstance.CurrentUser.Uid);
                var documents = await query.GetDocumentsAsync();

                List<SavedListing> savedListings = new List<SavedListing>();
                foreach (var doc in documents.Documents)
                {
                    var dictionary = doc.Data;
                    var savedListing = new SavedListing()
                    {
                        PropertyId = dictionary.ValueForKey(new NSString("propertyId")) as NSString,
                        MlsNumber = dictionary.ValueForKey(new NSString("mlsNumber")) as NSString,
                        Address = dictionary.ValueForKey(new NSString("mlsNumber")) as NSString,
                        Price = dictionary.ValueForKey(new NSString("price")) as NSString,
                        UserId = dictionary.ValueForKey(new NSString("userId")) as NSString,
                        Id = doc.Id

                    };
                    savedListings.Add(savedListing);
                }
                return savedListings;
            }
            catch
            {
                return new List<SavedListing>();
            }

        }



        public async Task<bool> Delete(SavedListing savedListing)
        {
            try
            {
                var collection = Firebase.CloudFirestore.Firestore.SharedInstance.GetCollection("savedListings");
                await collection.GetDocument(savedListing.Id).DeleteDocumentAsync();
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public bool Insert(SavedListing savedListing)
        {
            try
            {

                var keys = new[]
                {
                    
                    new NSString("propertyId"),
                    new NSString("mlsNumber"),
                    new NSString("address"),
                    new NSString("price"),
                    new NSString("userId")
                };
                var values = new NSObject[]
                {
                    new NSString(savedListing.PropertyId),
                    new NSString(savedListing.MlsNumber),
                    new NSString(savedListing.Address),
                    new NSString(savedListing.Price),
                    new NSString(Firebase.Auth.Auth.DefaultInstance.CurrentUser.Uid)
                };

                var document = new NSDictionary<NSString, NSObject>(keys, values);
                var collection = Firebase.CloudFirestore.Firestore.SharedInstance.GetCollection("savedListings");
                collection.AddDocument(document);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public async Task<bool> Update(SavedListing savedListing)
        {
            try
            {

                var keys = new[]
                {
                    new NSString("propertyId"),
                    new NSString("mlsNumber"),
                    new NSString("address"),
                    new NSString("price"),
                    new NSString("userId")
                };
                var values = new NSObject[]
                {
                    new NSString(savedListing.PropertyId),
                    new NSString(savedListing.MlsNumber),
                    new NSString(savedListing.Address),
                    new NSString(savedListing.Price),
                    new NSString(Firebase.Auth.Auth.DefaultInstance.CurrentUser.Uid)
                };

                var document = new NSDictionary<NSObject, NSObject>(keys, values);
                var collection = Firebase.CloudFirestore.Firestore.SharedInstance.GetCollection("savedListings");
                await collection.GetDocument(savedListing.Id).UpdateDataAsync(document);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
    }
}

