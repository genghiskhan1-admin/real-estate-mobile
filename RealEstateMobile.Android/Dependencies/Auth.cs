﻿using Firebase.Auth;
using RealEstateMobile.Consts;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;


[assembly: Dependency(typeof(RealEstateMobile.Droid.Dependencies.Auth))]
namespace RealEstateMobile.Droid.Dependencies
{
    public class Auth : IAuth
    {
        public Auth()
        {

        }

        public string GetCurrentUserId()
        {
            return FirebaseAuth.Instance.CurrentUser.Uid;
        }

        public bool IsAuthenticated()
        {
            return FirebaseAuth.Instance.CurrentUser != null;
        }

        public async Task<bool> LoginUser(string email, string password)
        {
            try
            {
                await FirebaseAuth.Instance.SignInWithEmailAndPasswordAsync(email, password);
                return true;
            }
            catch (FirebaseAuthWeakPasswordException ex)
            {
                throw new Exception(ex.Message);
            }
            catch(FirebaseAuthInvalidCredentialsException ex)
            {
                throw new Exception(ex.Message);
            }
            catch(FirebaseAuthInvalidUserException ex)
            {
                throw new Exception("There is no user record corresponding to this identifier");
            }
            catch (Exception)
            {
                throw new Exception("There wss an unknown error!");
            }
        }

        public async Task<bool> SignOut()
        {
            await Task.Run(() => FirebaseAuth.Instance.SignOut());
            return true;

        }

        public async Task<string> RegisterUser(string email, string password)
        {
            try
            {
                var result = await FirebaseAuth.Instance.CreateUserWithEmailAndPasswordAsync(email, password);
                return result.User.Uid;
            }
            catch (FirebaseAuthWeakPasswordException ex)
            {
                throw new Exception(ex.Message);
            }
            catch (FirebaseAuthInvalidCredentialsException ex)
            {
                throw new Exception(ex.Message);
            }
            catch (FirebaseAuthUserCollisionException ex)
            {
                throw new Exception(ex.Message);
            }
            catch (Exception)
            {
                throw new Exception("There wss an unknown error!");
            }
        }

        public string GetCurrentUserEmail()
        {
            return FirebaseAuth.Instance.CurrentUser.Email;
        }

        public async Task ResetPasswordLinkSent(string email)
        {
            await Task.Run(()=> FirebaseAuth.Instance.SendPasswordResetEmail(email));
        }
    }
}
