﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Android.Gms.Maps.Model;
using Android.Gms.Tasks;
using Firebase.Firestore;
using Java.Interop;
using Java.Util;
using RealEstateMobile.Consts;
using RealEstateMobile.Models;
using Xamarin.Forms;

[assembly: Dependency(typeof(RealEstateMobile.Droid.Dependencies.Firestore))]
namespace RealEstateMobile.Droid.Dependencies
{
    public class Firestore: Java.Lang.Object, IFirestore, IOnCompleteListener
    {

        List<SavedListing> savedListings;
        bool hasReadListings = false;

        public Firestore()
        {
            savedListings = new List<SavedListing>();
        }

        public async Task<bool> Delete(SavedListing savedListing)
        {
            try
            {
                var collection = Firebase.Firestore.FirebaseFirestore.Instance.Collection("savedListings");
                collection.Document(savedListing.Id).Delete();
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public bool Insert(SavedListing savedListing)
        {
            try
            {
                var userDocument = new Dictionary<string, Java.Lang.Object>
            {
                { "propertyId", savedListing.PropertyId },
                { "mlsNumber", savedListing.MlsNumber },
                { "address", savedListing.Address },
                { "price", savedListing.Price },
                { "userId", Firebase.Auth.FirebaseAuth.Instance.CurrentUser.Uid }
            };

                var collection = Firebase.Firestore.FirebaseFirestore.Instance.Collection("savedListings");
                collection.Add(new HashMap(userDocument));

                return true;
            }
            catch(Exception ex)
            {
                return false;
            }

        }

        public async Task<bool> Update(SavedListing savedListing)
        {
            try
            {
                var listingDocument = new Dictionary<string, Java.Lang.Object>
            {
                { "propertyId", savedListing.PropertyId },
                { "mlsNumber", savedListing.MlsNumber },
                { "address", savedListing.Address },
                { "price", savedListing.Price },
                { "userId", Firebase.Auth.FirebaseAuth.Instance.CurrentUser.Uid  }
            };

                var collection = Firebase.Firestore.FirebaseFirestore.Instance.Collection("savedListings");
                collection.Document(savedListing.Id).Update(listingDocument);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }



        public void OnComplete(Android.Gms.Tasks.Task task)
        {
            if (task.IsSuccessful)
            {
                var documents = (QuerySnapshot)task.Result;
                savedListings.Clear();
                foreach(var listing in documents.Documents)
                {
                    SavedListing newlySavedListing = new SavedListing()
                    {
                        PropertyId = listing.Get("propertyId").ToString(),
                        MlsNumber = listing.Get("mlsNumber").ToString(),
                        Price = listing.Get("price").ToString(),
                        Address = listing.Get("address").ToString(),
                        UserId = listing.Get("userId").ToString(),
                        Id = listing.Id

                    };
                    savedListings.Add(newlySavedListing);
                }

                
            }
            else
            {
                savedListings.Clear();
            }

            hasReadListings = true;
        }



        public async Task<List<SavedListing>> Read()
        {
            try
            {
                var collection = Firebase.Firestore.FirebaseFirestore.Instance.Collection("savedListings");
                var query = collection.WhereEqualTo("userId", Firebase.Auth.FirebaseAuth.Instance.CurrentUser.Uid);
                query.Get().AddOnCompleteListener(this);

                for (int i = 0; i < 50; i++)
                {
                    await System.Threading.Tasks.Task.Delay(100);
                    if (hasReadListings)
                        break;
                }

                return savedListings;
            }
            catch(Exception ex)
            {
                return savedListings;
            }
            
        }
    }
}

