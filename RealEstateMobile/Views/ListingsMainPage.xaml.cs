﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RealEstateMobile.Models;
using RealEstateMobile.Services;
using RealEstateMobile.ViewModels;
using RealEstateMobile.Views;
using Xamarin.Forms;



namespace RealEstateMobile.Views
{
    public partial class ListingsMainPage : ContentPage
    {

        private ListingsMainPageViewModel vm;
        public ListingsMainPage()
        {

            InitializeComponent();
            
            vm = Resources["vm"] as ListingsMainPageViewModel;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }
    }
}

