﻿using System;
using System.Collections.Generic;
using System.IO;
using RealEstateMobile.Consts;
using RealEstateMobile.Models;
using SQLite;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace RealEstateMobile.Views
{
    public partial class UserLoginPage : ContentPage
    {
        public UserLoginPage()
        {
            InitializeComponent();
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            if (Auth.IsAuthenticated())
            {
                await Shell.Current.GoToAsync("//main");
            }
        }
    }
}
