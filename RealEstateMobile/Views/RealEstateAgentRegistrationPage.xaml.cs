﻿using System;
using System.Collections.Generic;
using System.IO;
using RealEstateMobile.Models;
using SQLite;
using Xamarin.Forms;

namespace RealEstateMobile.Views
{
    public partial class RealEstateAgentRegistrationPage : ContentPage
    {
        public RealEstateAgentRegistrationPage()
        {
            InitializeComponent();
        }


        void Register_Button_Clicked(System.Object sender, System.EventArgs e)
        {

            var dbpath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "HOBDatabase.db");

            var db = new SQLiteConnection(dbpath);

            db.CreateTable<RealEstateAgent>();

            var item = new RealEstateAgent()
            {
                RealEstateAgentName = EntryRealEstateFullName.Text,
                RealEstateAgentAge = Convert.ToInt32(EntryRealEstateAge.Text),
                RealEstateAgentBrokrage = EntryRealEstateBrokrage.Text,
                RealEstateAgentDescription = EntryRealEstateDescription.Text
            };

            db.Insert(item);
            Device.BeginInvokeOnMainThread(async () =>
            {
                var result = await this.DisplayAlert("Congratulation", "User Registration Sucessful", "Yes", "Cancel");

                if (result)
                    await Navigation.PushModalAsync(new ListingsMainPage()); //can change this to view real estate agent directory page 
            });
        }
    }
}

