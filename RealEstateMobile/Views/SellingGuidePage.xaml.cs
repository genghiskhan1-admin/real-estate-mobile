﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace RealEstateMobile.Views
{
    public partial class SellingGuidePage : ContentPage
    {
        public SellingGuidePage()
        {
            InitializeComponent();
            MenuItems = GetMenus();
            this.BindingContext = this;

        }

        public ObservableCollection<Menu> MenuItems { get; set; }

        private ObservableCollection<Menu> GetMenus()
        {
            return new ObservableCollection<Menu>
            {
                new Menu { Title = "Main Listings", Icon = "profile.png" },
                new Menu { Title = "SignIn or SignUp", Icon = "feed.png" },
                new Menu { Title = "Agent Signup", Icon = "activity.png" },
                new Menu { Title = "View Agents", Icon = "settings.png" },
                new Menu { Title = "Buyers Guide", Icon = "settings.png" },
                new Menu { Title = "Sellers Guide", Icon = "settings.png" },
                new Menu { Title = "Post Private Listing", Icon = "settings.png" },
                //new Menu { Title = "View Private Listing", Icon = "settings.png" },
                new Menu { Title = "Settings", Icon = "settings.png" },
                new Menu { Title = "Favourites", Icon = "settings.png" }
            };
        }

        private async void Show()
        {
            _ = TitleTxt.FadeTo(0);
            _ = MenuItemsView.FadeTo(1);
            await MainMenuView.RotateTo(0, 300, Easing.BounceOut);
        }

        private async void Hide()
        {
            _ = TitleTxt.FadeTo(1);
            _ = MenuItemsView.FadeTo(0);
            await MainMenuView.RotateTo(-90, 300, Easing.BounceOut);

        }

        private void ShowMenu(object sender, EventArgs e)
        {
            Show();
        }

        private void MenuTapped(object sender, EventArgs e)
        {
            TitleTxt.Text = ((sender as StackLayout).BindingContext as Menu).Title;
            Hide();
            switch (((sender as StackLayout).BindingContext as Menu).Title)
            {
                case "Main Listings":
                    Navigation.PopModalAsync();
                    break;
                case "SignIn or SignUp":
                    Navigation.PushModalAsync(new UserLoginPage());
                    break;
                case "Agent Signup":
                    Navigation.PushModalAsync(new RealEstateAgentRegistrationPage());
                    break;
                case "View Agents":
                    Navigation.PopModalAsync();
                    break;
                case "Buyers Guide":
                    Navigation.PushModalAsync(new BuyingGuidePage());
                    break;
                case "Sellers Guide":
                    Navigation.PushModalAsync(new SellingGuidePage());
                    break;
                case "Post Private Listing":
                    Navigation.PopModalAsync();
                    break;
                case "View Private Listing":
                    Navigation.PopModalAsync();
                    break;
                case "Settings":
                    Navigation.PushModalAsync(new SettingPage());
                    break;
                case "Favourites":
                    Navigation.PushModalAsync(new FavouritedListings());
                    break;
            }

        }

    }


}

