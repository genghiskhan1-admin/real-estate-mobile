﻿using System;
using System.Collections.Generic;
using RealEstateMobile.Consts;
using RealEstateMobile.ViewModels;
using Xamarin.Forms;

namespace RealEstateMobile.Views
{
    public partial class SavedListings : ContentPage
    {
        Image rightImage;

        private SavedListingsViewModel vm;
        public SavedListings()
        {
            InitializeComponent();

            vm = Resources["vm"] as SavedListingsViewModel;
        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();

            await vm.loadSavedListings();
        }

    }


}

