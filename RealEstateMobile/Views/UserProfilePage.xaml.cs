﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEstateMobile.Consts;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using RealEstateMobile.ViewModels;

namespace RealEstateMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserProfilePage : ContentPage
    {
        public UserProfilePage()
        {
            InitializeComponent();
            BindingContext = new UserProfilePageViewModel();
        }

        protected override void OnAppearing()
        {
            if (BindingContext is ViewModels.UserProfilePageViewModel vm)
            {

                vm.GetCurrentUserEmail();
            }
            base.OnAppearing();
        }

    }
}