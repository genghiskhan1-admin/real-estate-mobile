﻿using System;
using System.Collections.Generic;
using Xamarin.Forms;

namespace RealEstateMobile.Views
{
    public partial class LoginMethodsPage : ContentPage
    {
        public LoginMethodsPage()
        {
            InitializeComponent();
        }

        async void Login_Button_Clicked(System.Object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new UserLoginPage());
        }

        async void SignUp_Button_Clicked(System.Object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new UserRegistrationPage());
        }
    }
}

