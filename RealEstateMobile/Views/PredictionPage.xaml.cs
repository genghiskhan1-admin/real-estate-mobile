﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using RealEstateMobile.Views;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using Xamarin.Essentials;
using Xamarin.Forms.Maps;
using System.Net.Http;
using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;

namespace RealEstateMobile.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PredictionPage : ContentPage
    {
        public PredictionPage()
        {
            InitializeComponent();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            GetLocation();
        }

        private async void GetLocation()
        {
            var status = await CheckAndRequestLocationPermission();

            if(status == PermissionStatus.Granted)
            {
                var location = await Geolocation.GetLocationAsync();

                locationsMap.IsShowingUser = true;

                CenterMap(location.Latitude, location.Longitude);
            }
        }

        private void CenterMap(double latitude, double longitude)
        {
            Position center = new Position(latitude, longitude);
            MapSpan span = new MapSpan(center, 1, 1);
            locationsMap.MoveToRegion(span);
        }

        private async Task<PermissionStatus> CheckAndRequestLocationPermission()
        {
            var status = await Permissions.CheckStatusAsync<Permissions.LocationWhenInUse>();

            if(status == PermissionStatus.Granted)
            {
                return status;
            }

            if(status == PermissionStatus.Denied && DeviceInfo.Platform == DevicePlatform.iOS)
            {
                return status;
            }

            status = await Permissions.RequestAsync<Permissions.LocationWhenInUse>();
            return status;
        }

        
        void locationsMap_PropertyChanged(System.Object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {

                Xamarin.Forms.Maps.Map m = (Xamarin.Forms.Maps.Map)sender;
                if (m.VisibleRegion != null)
                {
                    UserLatitude.Text = m.VisibleRegion.Center.Latitude.ToString();
                    UserLongitude.Text = m.VisibleRegion.Center.Longitude.ToString();
                }
                      
            
        }

        async void Prediction_Button_Clicked(System.Object sender, System.EventArgs e)
        {
            var result = await GetPrediction();
            PredictionResult.Text = result;
        }


        public async Task<string> GetPrediction()
        {
            string message = string.Empty;
            HttpClient client = new HttpClient();
            var url = "https://arcane-shelf-01474.herokuapp.com/";


            string contentType = "application/json";
            JObject jsonData = new JObject
            {
                { "Latitude", float.Parse(UserLatitude.Text) },
                { "Longitude", float.Parse(UserLongitude.Text) },
                {"LandSize", Convert.ToInt32(SliderValueLandSize.Value)},
                {"SizeInterior", Convert.ToInt32(SliderValueInteriorSize.Value)},
                {"Bedrooms", Convert.ToInt32(SliderValueNumBedrooms.Value)},
                {"Bathrooms", Convert.ToInt32(SliderValueNumBathrooms.Value)}
            };

            var content = new StringContent(jsonData.ToString(), Encoding.UTF8, contentType);

            var httpResponseMessage = await client.PostAsync(url, content);

            
            if (httpResponseMessage != null)
            {
                message = await httpResponseMessage.Content.ReadAsStringAsync();
                var obj = JObject.Parse(message.ToString());
                var outputPredictedPrice = "";
                var predictedPrice = obj["Prediction"].ToString();
                if(predictedPrice != null)
                {
                    outputPredictedPrice = new string(predictedPrice.Where(c => char.IsDigit(c)).ToArray());
                }

                return String.Format(new System.Globalization.CultureInfo("en-US"), "{0:C}" , Int32.Parse(outputPredictedPrice));
            }
            return "No Result";

        }

        void Slider_ValueChanged(System.Object sender, Xamarin.Forms.ValueChangedEventArgs e)
        {
            int value = Convert.ToInt32(e.NewValue);
            NumOfBedrooms.Text = value.ToString();
        }

        void SliderValueNumBathrooms_ValueChanged(System.Object sender, Xamarin.Forms.ValueChangedEventArgs e)
        {
            int value = Convert.ToInt32(e.NewValue);
            NumOfBathrooms.Text = value.ToString();
        }

        void SliderValueLandSize_ValueChanged(System.Object sender, Xamarin.Forms.ValueChangedEventArgs e)
        {
            int value = Convert.ToInt32(e.NewValue);
            LandSize.Text = value.ToString();
        }


        void SliderValueInteriorSize_ValueChanged(System.Object sender, Xamarin.Forms.ValueChangedEventArgs e)
        {
            int value = Convert.ToInt32(e.NewValue);
            InteriorSize.Text = value.ToString();
        }

        private void Stats_Button_Clicked(object sender, EventArgs e)
        {
            Shell.Current.GoToAsync($"{nameof(Views.ChartsPage)}?Latitude={UserLatitude.Text}&Longitude={UserLongitude.Text}");
        }
    }
}