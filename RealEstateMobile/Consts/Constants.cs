﻿using System;
namespace RealEstateMobile.Consts
{
    public class Constants
    {

        public const string RapidApiKey = "56130de0d1mshfea78c639c86253p118e95jsnc546d8ddb45e";
        public const string RapidApiHost = "realty-in-ca1.p.rapidapi.com";

        public const string LISTING_DETAIL_SEARCH = "https://realty-in-ca1.p.rapidapi.com/properties/detail?ReferenceNumber={0}&PropertyID={1}&PreferedMeasurementUnit=1&CultureId=1";
        public const string STATS_BY_COORDINATES = "https://realty-in-ca1.p.rapidapi.com/properties/get-statistics?Longitude={0}&Latitude={1}&CultureId=1";

    }
}