﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;
using RealEstateMobile.Consts;

namespace RealEstateMobile.Consts
{

    public interface IAuth
    {
        Task<string> RegisterUser(string email, string password);
        Task<bool> LoginUser(string email, string password);
        bool IsAuthenticated();
        string GetCurrentUserId();
        string GetCurrentUserEmail();

        Task ResetPasswordLinkSent(string email);
        Task<bool> SignOut();
    }
    public class Auth
    {
        private static IAuth auth = DependencyService.Get<IAuth>();

        public async static Task<string> RegisterUser(string email, string password)
        { 
            try
            {
                return await auth.RegisterUser(email, password);
            }
            catch (Exception ex)
            {
                await App.Current.MainPage.DisplayAlert("Error", ex.Message, "OK");

                return null;
            }
        }

        public async static Task<bool> SignOut()
        {
            var result = await auth.SignOut();
            return result;
        }

        public async static Task<bool> LoginUser(string email, string password)
        {
            try
            {
                return await auth.LoginUser(email, password);
            }
            catch(Exception ex)
            {
                await App.Current.MainPage.DisplayAlert("Error", ex.Message, "OK");

                return false;
            }
        }

        public static bool IsAuthenticated()
        {
            return auth.IsAuthenticated();
        }

        public static string GetCurrentUserId()
        {
            return auth.GetCurrentUserId();
        }

        public static string GetCurrentUserEmail()
        {
            return auth.GetCurrentUserEmail();
        }

        public async static Task ResetPasswordLinkSent(string email)
        {
            await auth.ResetPasswordLinkSent(email);
        }
    }
}
