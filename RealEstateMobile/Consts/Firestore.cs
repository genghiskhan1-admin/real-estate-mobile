﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using RealEstateMobile.Models;
using Xamarin.Forms;

namespace RealEstateMobile.Consts
{
    public interface IFirestore
    {
        bool Insert(SavedListing savedListing);
        Task<bool> Delete(SavedListing savedListing);
        Task<bool> Update(SavedListing savedListing);
        Task<List<SavedListing>> Read();

    }
    public class Firestore
    {

        private static IFirestore firestore = DependencyService.Get<IFirestore>();

        public static bool Insert(SavedListing savedListing)
        {
            return firestore.Insert(savedListing);
        }

        public static async Task<bool> Update(SavedListing savedListing)
        {
            return await firestore.Update(savedListing);
        }

        public static async Task<bool> Delete(SavedListing savedListing)
        {
            return await firestore.Delete(savedListing);
        }

        public static async Task<List<SavedListing>> Read()
        {
            return await firestore.Read();
        }
    }
}

