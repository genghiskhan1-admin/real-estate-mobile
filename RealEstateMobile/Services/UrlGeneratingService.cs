﻿using RealEstateMobile.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace RealEstateMobile.Services
{
    public class UrlGeneratingService
    {
        public static string GenerateSearchDetailUrl(string mlsNumber, string propertyId)
        {
            return string.Format(Consts.Constants.LISTING_DETAIL_SEARCH, mlsNumber, propertyId);
        }

        public static string GenerateStatsUrl(string longitude, string latitude)
        {
            return string.Format(Consts.Constants.STATS_BY_COORDINATES, longitude, latitude);
        }
    }
}
