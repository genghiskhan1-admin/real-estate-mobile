﻿using RealEstateMobile.Validators;
using RealEstateMobile.Validators.Rules;
using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace RealEstateMobile.ViewModels
{
    public class UserResetPasswordPageViewModel: LoginViewModel
    {
        public UserResetPasswordPageViewModel()
        {
            this.EmailSendLinkCommand = new Command(this.EmailSendLinkClicked);
            this.RedirectToLoginCommand = new Command(this.RedirectToLoginClicked);
        }

        public Command EmailSendLinkCommand { get; set; }
        public Command RedirectToLoginCommand { get; set; }


        private async void EmailSendLinkClicked(object obj)
        {
            if (this.IsEmailFieldValid())
            {
                try
                {
                    await Consts.Auth.ResetPasswordLinkSent(this.Email.Value);
                    await App.Current.MainPage.DisplayAlert("Success", "Reset password link Successfully sent, check your email!", "Ok");
                }
                catch (Exception e)
                {
                    await App.Current.MainPage.DisplayAlert("Error", "Unexpected error occurred!", "Ok");
                }
            }
        }

        private async void RedirectToLoginClicked(object obj)
        {
            await Shell.Current.GoToAsync("//login");
        }


        private async void nagivateToMain(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync("//main");
        }

    }



    
}
