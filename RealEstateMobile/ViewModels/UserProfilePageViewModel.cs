﻿using System;
using Xamarin.Forms;

namespace RealEstateMobile.ViewModels
{
	public class UserProfilePageViewModel : BindableObject
    {
		public UserProfilePageViewModel()
		{
            this.LogoutCommand = new Command(this.LogoutClicked);
            this.SeeSavedListingsCommand = new Command(this.SaveListingsClicked);
        }

        /// <summary>
        /// Gets or sets the command that is executed when the Log out button is clicked.
        /// </summary>
        public Command LogoutCommand { get; set; }

        public Command SeeSavedListingsCommand { get; set; }


        public string email { get; set; }

        public string Email
        {
            get { return email; }
            set
            {
                email = value;
                OnPropertyChanged("Email");
            }
        }

        /// <summary>
        /// Initializing the properties.
        /// </summary>
        public void GetCurrentUserEmail()
        {
            this.Email = Consts.Auth.GetCurrentUserEmail();
        }

        /// <summary>
        /// Invoked when the Log In button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private async void LogoutClicked(object obj)
        {
            var result = await Consts.Auth.SignOut();
            if (result)
            {
                await Shell.Current.GoToAsync("//login");
            }
        }

        private async void SaveListingsClicked(object obj)
        {
            await Shell.Current.GoToAsync($"{nameof(Views.SavedListings)}");
        }
    }
}

