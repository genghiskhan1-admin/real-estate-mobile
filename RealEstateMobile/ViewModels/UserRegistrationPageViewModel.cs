﻿using RealEstateMobile.Validators;
using RealEstateMobile.Validators.Rules;
using Xamarin.Forms;
using Xamarin.Forms.Internals;
using RealEstateMobile.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.ComponentModel;
using Syncfusion.XForms.PopupLayout;
using System.Threading.Tasks;

namespace RealEstateMobile.ViewModels
{
    /// <summary>
    /// ViewModel for sign-up page.
    /// </summary>
    [Preserve(AllMembers = true)]
    public class UserRegistrationPageViewModel : LoginViewModel
    {
        #region Fields

        private ValidatablePair<string> password;

        private SfPopupLayout sfPopupLayout;

        private DataTemplate templateView;

        private Label popupContent;


        #endregion

        #region Constructor

        /// <summary>
        /// Initializes a new instance for the <see cref="SignUpPageViewModel" /> class.
        /// </summary>
        public UserRegistrationPageViewModel()
        {
            this.InitializeProperties();
            this.AddValidationRules();
            
            this.RedirectToLoginCommand = new Command(this.RedirectToLoginClicked);
            this.SignUpCommand = new Command(this.SignUpClicked);
            
        }
        #endregion

        #region Property

        /// <summary>
        /// Gets or sets the property that bounds with an entry that gets the password from users in the Sign Up page.
        /// </summary>
        public ValidatablePair<string> Password
        {
            get
            {
                return this.password;
            }

            set
            {
                if (this.password == value)
                {
                    return;
                }

                this.SetProperty(ref this.password, value);
            }
        }


        #endregion

        #region Command

        /// <summary>
        /// Gets or sets the command that is executed when the Log In button is clicked.
        /// </summary>
        public Command RedirectToLoginCommand { get; set; }

        /// <summary>
        /// Gets or sets the command that is executed when the Sign Up button is clicked.
        /// </summary>
        public Command SignUpCommand { get; set; }


        #endregion

        #region Methods

        /// <summary>
        /// Initialize whether fieldsvalue are true or false.
        /// </summary>
        /// <returns>true or false </returns>
        public bool AreFieldsValid()
        {
            bool isEmail = this.Email.Validate();
            bool isPasswordValid = this.Password.Validate();
            return isPasswordValid  && isEmail;
        }

        /// <summary>
        /// Initializing the properties.
        /// </summary>
        private void InitializeProperties()
        {
            this.Password = new ValidatablePair<string>();
        }

        /// <summary>
        /// this method contains the validation rules
        /// </summary>
        private void AddValidationRules()
        {
            this.Password.Item1.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Password Required" });
            this.Password.Item2.Validations.Add(new IsNotNullOrEmptyRule<string> { ValidationMessage = "Re-enter Password" });
        }

        /// <summary>
        /// Invoked when the Log in button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private async void RedirectToLoginClicked(object obj)
        {
            await Shell.Current.GoToAsync("//login");
        }

        /// <summary>
        /// Invoked when the Sign Up button is clicked.
        /// </summary>
        /// <param name="obj">The Object</param>
        private async void SignUpClicked(object obj)
        {
            if (this.AreFieldsValid())
            {
                var result = await Consts.Auth.RegisterUser(this.Email.Value, this.Password.Item1.Value);


                if(result != null)
                {
                    await App.Current.MainPage.DisplayAlert("Success", "Resgitration Success!", "Ok");
                    await Shell.Current.GoToAsync("//main");
                }
                
            }

        }


        private async void nagivateToMain(object sender, EventArgs e)
        {
            await Shell.Current.GoToAsync("//main");
        }
        #endregion
    }
}