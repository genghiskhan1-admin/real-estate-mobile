﻿using Newtonsoft.Json.Linq;
using RealEstateMobile.Models;
using RealEstateMobile.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using Xamarin.Forms;

namespace RealEstateMobile.ViewModels
{
    public class ChartsPageViewModel : BindableObject, IQueryAttributable
    {

        public ObservableCollection<PopByAgeGroup> popByAgeGroupCollection { get; set; }
        public ObservableCollection<PopTrendByYear> popTrendByYearCollection { get; set; }
        public ObservableCollection<OwnershipTypeStats> ownershipTypeStatsCollection { get; set; }
        public ObservableCollection<ConstructionDateStats> constructionDateStatsCollection { get; set; }
        public ObservableCollection<MaritalStatusStats> maritalStatusStatsCollection { get; set; }
        public ObservableCollection<EducationStats> educationStatsCollection { get; set; }
        public ObservableCollection<HouseholdIncomeStats> householdIncomeStatsCollection { get; set; }
        public ObservableCollection<ChildrenAtHomeStats> childrenAtHomeStatsCollection { get; set; }
        public ObservableCollection<LanguagesStats> languagesStatsCollection { get; set; }
        public ObservableCollection<OccupationsStats> occupationsStatsCollection { get; set; }

        public ChartsPageViewModel()
        {
            popByAgeGroupCollection = new ObservableCollection<PopByAgeGroup>();
            popTrendByYearCollection = new ObservableCollection<PopTrendByYear>();
            ownershipTypeStatsCollection = new ObservableCollection<OwnershipTypeStats>();
            constructionDateStatsCollection = new ObservableCollection<ConstructionDateStats>(); 
            maritalStatusStatsCollection = new ObservableCollection<MaritalStatusStats>();
            educationStatsCollection = new ObservableCollection<EducationStats>();
            householdIncomeStatsCollection = new ObservableCollection<HouseholdIncomeStats>();
            childrenAtHomeStatsCollection = new ObservableCollection<ChildrenAtHomeStats>();
            languagesStatsCollection = new ObservableCollection<LanguagesStats>();
            occupationsStatsCollection = new ObservableCollection<OccupationsStats>();
             
        }

        public string latitude { get; set; }
        public string Latitude
        {
            get { return latitude; }
            set
            {
                latitude = value;
                OnPropertyChanged("Latitude");
            }
        }

        public string longitude { get; set; }
        public string Longitude
        {
            get { return longitude; }
            set
            {
                longitude = value;
                OnPropertyChanged("Longitude");
            }
        }


        public async Task LoadData()
        {
            try
            {
                popByAgeGroupCollection.Clear();
                popTrendByYearCollection.Clear();
                ownershipTypeStatsCollection.Clear();
                constructionDateStatsCollection.Clear();
                maritalStatusStatsCollection.Clear();
                educationStatsCollection.Clear();
                householdIncomeStatsCollection.Clear();
                childrenAtHomeStatsCollection.Clear();
                languagesStatsCollection.Clear();
                occupationsStatsCollection.Clear();

                var client = new HttpClient();
                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri(UrlGeneratingService.GenerateStatsUrl(Longitude, Latitude)),
                    Headers =
                {
                    { "X-RapidAPI-Key", "56130de0d1mshfea78c639c86253p118e95jsnc546d8ddb45e" },
                    { "X-RapidAPI-Host", "realty-in-ca1.p.rapidapi.com" },
                },
                };
                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var body = await response.Content.ReadAsStringAsync();


                    var obj = JObject.Parse(body.ToString());

                    var popByAgeGroupData = obj["Data"][2]["value"];
                    foreach (var item in popByAgeGroupData)
                    {
                        var ageGroup = item["key"].ToString();

                        popByAgeGroupCollection.Add(new PopByAgeGroup { AgeGroup = ageGroup.Substring(0, ageGroup.Length - 10), AgeGroupValue = Int32.Parse(item["value"].ToString()) });
                    }
                    

                    var popTrendByYearData = obj["Data"][3]["value"];
                    foreach (var item in popTrendByYearData)
                    {
                        popTrendByYearCollection.Add(new PopTrendByYear { Year = item["key"].ToString(), PopTrendByYearValue = Int32.Parse(item["value"].ToString()) });
                    }
                    

                    var ownershipTypeStatsDta = obj["Data"][9]["value"];
                    foreach (var item in ownershipTypeStatsDta)
                    {
                        ownershipTypeStatsCollection.Add(new OwnershipTypeStats { OwnershipType = item["key"].ToString(), OwnershipTypeValue = Int32.Parse(item["value"].ToString()) });
                    }
                    
                    var constructionStatsData = obj["Data"][10]["value"];
                    foreach (var item in constructionStatsData)
                    {
                        constructionDateStatsCollection.Add(new ConstructionDateStats { TimePeriod = item["key"].ToString(), TimePeriodValue = Int32.Parse(item["value"].ToString()) });
                    }

                    var martialStatusStatsData = obj["Data"][5]["value"];
                    foreach (var item in martialStatusStatsData)
                    {
                        maritalStatusStatsCollection.Add(new MaritalStatusStats { MartialStatus = item["key"].ToString(), MartialStatusValue = Int32.Parse(item["value"].ToString()) });
                    }

                    var educationStatsData = obj["Data"][4]["value"];
                    foreach (var item in educationStatsData)
                    {
                        educationStatsCollection.Add(new EducationStats { Education = item["key"].ToString(), EducationValue = Int32.Parse(item["value"].ToString()) });
                    }

                    var householdIncomeStatsData = obj["Data"][7]["value"];
                    foreach (var item in householdIncomeStatsData)
                    {
                        householdIncomeStatsCollection.Add(new HouseholdIncomeStats { HouseholdIncome = item["key"].ToString(), HouseholdIncomeValue = Int32.Parse(item["value"].ToString()) });
                    }

                    var childrenAtHomeStatsData = obj["Data"][8]["value"];
                    foreach (var item in childrenAtHomeStatsData)
                    {
                        var childrenAtHome = item["key"].ToString();

                        childrenAtHomeStatsCollection.Add(new ChildrenAtHomeStats { ChildrenAtHome = childrenAtHome.Substring(0, childrenAtHome.Length - 10), ChildrenAtHomeValue = Int32.Parse(item["value"].ToString()) });
                    }

                    var languagesStatsData = obj["Data"][6]["value"];
                    foreach (var item in languagesStatsData)
                    {
                        languagesStatsCollection.Add(new LanguagesStats { Languages = item["key"].ToString(), LanguagesValue = Int32.Parse(item["value"].ToString()) });
                    }

                    var occupationsStatsData = obj["Data"][11]["value"];
                    foreach (var item in occupationsStatsData)
                    {
                        occupationsStatsCollection.Add(new OccupationsStats { Occupations = item["key"].ToString(), OccupationsValue = Int32.Parse(item["value"].ToString()) });
                    }
                }

            }
            catch (IndexOutOfRangeException e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error: " + e.Message);
            }
        }

        public void ApplyQueryAttributes(IDictionary<string, string> query)
        {
            Latitude = HttpUtility.UrlDecode(query["Latitude"]);
            Longitude = HttpUtility.UrlDecode(query["Longitude"]);
            Console.WriteLine("Latitude: " + Latitude);
            Console.WriteLine("Longitude: " + Longitude);
            _ = LoadData();
        }
    }
}
