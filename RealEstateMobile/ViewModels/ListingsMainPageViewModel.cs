﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RealEstateMobile.Consts;
using RealEstateMobile.Models;
using RealEstateMobile.Services;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace RealEstateMobile.ViewModels
{
    public class ListingsMainPageViewModel : BaseViewModel
    {

        public ObservableCollection<Result> Results { get; set; }

        public Command<object> ItemTappedCommand { get; set; }

        internal Result TappedItem { get; set; }


        public ListingsMainPageViewModel()
        {

            Results = new ObservableCollection<Result>();
            ItemTappedCommand = new Command<object>(TappedCommandMethod);
            _ = LoadHouseListings();
        }

        private async void TappedCommandMethod(object obj)
        {
            var ItemData = (obj as Syncfusion.ListView.XForms.ItemTappedEventArgs).ItemData as Result;
                
            if(this.TappedItem == null)
            {
                this.TappedItem = ItemData;
            }
            else
            {
                if (ItemData != this.TappedItem)
                {
                    this.TappedItem = ItemData;
                }
                else
                {
                    this.TappedItem = null;
                }
            }

            await Shell.Current.GoToAsync($"{nameof(Views.ListingDetail)}?PropertyId={ItemData.Id}&MlsNumber={ItemData.MlsNumber}");
        }


        private async void GetLocation()
        {
            var status = await CheckAndRequestLocationPermission();

            if (status == PermissionStatus.Granted)
            {
                var location = await Geolocation.GetLocationAsync();
                Console.WriteLine("The location is "+ location);
            }
        }

        private async Task<PermissionStatus> CheckAndRequestLocationPermission()
        {
            var status = await Permissions.CheckStatusAsync<Permissions.LocationWhenInUse>();

            if (status == PermissionStatus.Granted)
            {
                return status;
            }

            if (status == PermissionStatus.Denied && DeviceInfo.Platform == DevicePlatform.iOS)
            {
                return status;
            }

            status = await Permissions.RequestAsync<Permissions.LocationWhenInUse>();
            return status;
        }

        public async Task LoadHouseListings()
        {
            GetLocation();
            Results.Clear();

            var sortBy = "6";
            var client = new HttpClient();
            var request = new HttpRequestMessage
            {
                Method = HttpMethod.Get,
                RequestUri = new Uri("https://realty-in-ca1.p.rapidapi.com/properties/list-residential?LatitudeMax=81.14747595814636&LatitudeMin=-22.26872153207163&LongitudeMax=-10.267941690981388&LongitudeMin=-136.83037765324116&CurrentPage=1&RecordsPerPage=40&SortOrder=D&SortBy=6&CultureId=1&NumberOfDays=0&BedRange=0-0&BathRange=0-0&RentMin=0&BuildingTypeId=1"),
                Headers =
                {
                    { "X-RapidAPI-Key", Constants.RapidApiKey },
                    { "X-RapidAPI-Host", Constants.RapidApiHost },
                },
            };
            using (var response = await client.SendAsync(request))
            {
                response.EnsureSuccessStatusCode();
                var body = await response.Content.ReadAsStringAsync();

                var obj = JObject.Parse(body.ToString());

                var resultsFrom = obj["Results"].ToObject<List<Result>>();


                Console.WriteLine("Result is: " + resultsFrom[0].MlsNumber);

                foreach (var listing in resultsFrom)
                {
                    Results.Add(listing);
                }
            };
        }



    }
}
