﻿using Newtonsoft.Json;
using RealEstateMobile.Consts;
using RealEstateMobile.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace RealEstateMobile.ViewModels
{
    internal class SavedListingsViewModel : BaseViewModel
    {

        public ObservableCollection<SavedListing> SavedListings { get; set; }

        public Command<object> ItemTappedCommand { get; set; }

        public Command<object> DeleteCommand { get; set; }

        internal SavedListing TappedItem { get; set; }

        public SavedListingsViewModel()
        {
            SavedListings = new ObservableCollection<SavedListing>();
            ItemTappedCommand = new Command<object>(TappedCommandMethod);
            DeleteCommand = new Command<object>(OnTapped);
        }

        private async void OnTapped(object obj)
        {
            var savedListing = obj as SavedListing;
            Console.WriteLine(savedListing.MlsNumber + " is Deleted!!!!!!");
            await Firestore.Delete(savedListing);
            SavedListings.Remove(savedListing);
;           
        }


        private async void TappedCommandMethod(object obj)
        {
            var ItemData = (obj as Syncfusion.ListView.XForms.ItemTappedEventArgs).ItemData as SavedListing;

            if (this.TappedItem == null)
            {
                this.TappedItem = ItemData;
            }
            else
            {
                if (ItemData != this.TappedItem)
                {
                    this.TappedItem = ItemData;
                }
                else
                {
                    this.TappedItem = null;
                }
            }
 
            await Shell.Current.GoToAsync($"{nameof(Views.ListingDetail)}?PropertyId={ItemData.PropertyId}&MlsNumber={ItemData.MlsNumber}");
        }



        public async Task loadSavedListings()
        {
            SavedListings.Clear();
            var listings = await Firestore.Read();
            foreach (var listing in listings)
            {
                SavedListings.Add(listing);
                Console.WriteLine(listing.MlsNumber);
            }
        }
    }
}
