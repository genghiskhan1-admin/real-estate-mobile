﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RealEstateMobile.Consts;
using RealEstateMobile.Models;
using RealEstateMobile.Services;
using Syncfusion.XForms.PopupLayout;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web;
using Xamarin.Forms;

namespace RealEstateMobile.ViewModels
{
    public class ListingDetailViewModel : BindableObject, IQueryAttributable
    {

        public Result result { get; set; }
        private SfPopupLayout sfPopupLayout;
        private DataTemplate templateView;
        private Label popupContent;
        public ListingDetailViewModel()
        {
            this.SaveListingCommand = new Command(this.SaveListing);
        }


        public string propertyId { get; set; }

        public string PropertyId
        {
            get { return propertyId; }
            set
            {
                propertyId = value;
                OnPropertyChanged("PropertyId");
            }
        }

        public string mlsNumber { get; set; }

        public string MlsNumber
        {
            get { return mlsNumber; }
            set
            {
                mlsNumber = value;
                OnPropertyChanged("MlsNumber");
            }
        }

        public string address { get; set; }

        public string Address
        {
            get { return address; }
            set
            {
                address = value;
                OnPropertyChanged("Address");
            }
        }

        public string imgSrc { get; set; }

        public string ImgSrc
        {
            get { return imgSrc; }
            set
            {
                imgSrc = value;
                OnPropertyChanged("ImgSrc");
            }
        }

        public string price { get; set; }

        public string Price
        {
            get { return price; }
            set
            {
                price = value;
                OnPropertyChanged("Price");
            }
        }

        public string publicRemark { get; set; }

        public string PublicRemark
        {
            get { return publicRemark; }
            set
            {
                publicRemark = value;
                OnPropertyChanged("PublicRemark");
            }
        }

        public string bathroomTotal { get; set; }

        public string BathroomTotal
        {
            get { return bathroomTotal; }
            set
            {
                bathroomTotal = value;
                OnPropertyChanged("BathroomTotal");
            }
        }

        public string bedrooms { get; set; }

        public string Bedrooms
        {
            get { return bedrooms; }
            set
            {
                bedrooms = value;
                OnPropertyChanged("Bedrooms");
            }
        }

        public string houseType { get; set; }

        public string HouseType
        {
            get { return houseType; }
            set
            {
                houseType = value;
                OnPropertyChanged("HouseType");
            }
        }












        public string ammenities { get; set; }

        public string Ammenities
        {
            get { return ammenities; }
            set
            {
                ammenities = value;
                OnPropertyChanged("Ammenities");
            }
        }


        public string appliances { get; set; }

        public string Appliances
        {
            get { return appliances; }
            set
            {
                appliances = value;
                OnPropertyChanged("Appliances");
            }
        }

        public string architecturalStyle { get; set; }

        public string ArchitecturalStyle
        {
            get { return architecturalStyle; }
            set
            {
                architecturalStyle = value;
                OnPropertyChanged("ArchitecturalStyle");
            }
        }

        public string basementDevelopment { get; set; }

        public string BasementDevelopment
        {
            get { return basementDevelopment; }
            set
            {
                basementDevelopment = value;
                OnPropertyChanged("BasementDevelopment");
            }
        }

        public string basementFeatures { get; set; }

        public string BasementFeatures
        {
            get { return basementFeatures; }
            set
            {
                basementFeatures = value;
                OnPropertyChanged("BasementFeatures");
            }
        }

        public string basementType { get; set; }

        public string BasementType
        {
            get { return basementType; }
            set
            {
                basementType = value;
                OnPropertyChanged("BasementType");
            }
        }

        public string constructedDate { get; set; }

        public string ConstructedDate
        {
            get { return constructedDate; }
            set
            {
                constructedDate = value;
                OnPropertyChanged("ConstructedDate");
            }
        }


        public string sizeInterior { get; set; }

        public string SizeInterior
        {
            get { return sizeInterior; }
            set
            {
                sizeInterior = value;
                OnPropertyChanged("SizeInterior");
            }
        }

        public string landSize { get; set; }

        public string LandSize
        {
            get { return landSize; }
            set
            {
                landSize = value;
                OnPropertyChanged("LandSize");
            }
        }

        public string lastUpdated { get; set; }

        public string LastUpdated
        {
            get { return lastUpdated; }
            set
            {
                lastUpdated = value;
                OnPropertyChanged("LastUpdated");
            }
        }

        public string uploadBy { get; set; }

        public string UploadBy
        {
            get { return uploadBy; }
            set
            {
                uploadBy = value;
                OnPropertyChanged("UploadBy");
            }
        }
        public string parkingSpaceTotal { get; set; }

        public string ParkingSpaceTotal
        {
            get { return parkingSpaceTotal; }
            set
            {
                parkingSpaceTotal = value;
                OnPropertyChanged("ParkingSpaceTotal");
            }
        }



        public Command SaveListingCommand { get; set; }

        public async void LoadSavedListingDetail()
        {
            try
            {
                var client = new HttpClient();
                var url2 = UrlGeneratingService.GenerateSearchDetailUrl(MlsNumber, PropertyId);
                Console.WriteLine("whiy" + url2);
                var request = new HttpRequestMessage
                {
                    Method = HttpMethod.Get,
                    RequestUri = new Uri(url2),
                    Headers =
                {
                    { "X-RapidAPI-Key", Constants.RapidApiKey },
                    { "X-RapidAPI-Host", Constants.RapidApiHost },
                },
                };
                using (var response = await client.SendAsync(request))
                {
                    response.EnsureSuccessStatusCode();
                    var body = await response.Content.ReadAsStringAsync();

                    var obj = JObject.Parse(body.ToString());

                    var resultsFrom = obj.ToObject<ListingDetail>();


                    Console.WriteLine("Result is: " + resultsFrom.MlsNumber + resultsFrom.LastUpdated);
                    MlsNumber = resultsFrom.MlsNumber ?? "N/A";
                    
                    Address = resultsFrom.Property.Address.AddressText ?? "N/A";
                    PropertyId = resultsFrom.Id ?? "N/A";
                    Price = resultsFrom.Property.Price;
                    PublicRemark = resultsFrom.PublicRemarks ?? "N/A";
                    ImgSrc = resultsFrom.Property.Photo[0].HighResPath ?? "N/A";
                    BathroomTotal = resultsFrom.Building.BathroomTotal ?? "N/A";
                    Bedrooms = resultsFrom.Building.Bedrooms ?? "N/A";
                    HouseType = resultsFrom.Building.Type ?? "N/A";
                    LandSize = resultsFrom.Land.SizeTotal ?? "N/A";
                    SizeInterior = resultsFrom.Building.SizeInterior ?? "N/A";
                    LastUpdated = resultsFrom.LastUpdated ?? "N/A";
                    UploadBy = resultsFrom.UploadedBy ?? "N/A";
                    Ammenities = resultsFrom.Building.Ammenities ?? "N/A";
                    Appliances = resultsFrom.Building.Appliances ?? "N/A";
                    BasementDevelopment = resultsFrom.Building.BasementDevelopment ?? "N/A";
                    BasementType = resultsFrom.Building.BasementType ?? "N/A";
                    ConstructedDate = resultsFrom.Building.ConstructedDate ?? "N/A";
                };
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }



        public void ApplyQueryAttributes(IDictionary<string, string> query)
        {
            PropertyId = HttpUtility.UrlDecode(query["PropertyId"]);
            MlsNumber = HttpUtility.UrlDecode(query["MlsNumber"]);
            Console.WriteLine("Property Id: " + PropertyId);
            Console.WriteLine("MLSNumber: " + MlsNumber);
            LoadSavedListingDetail();
        }



        private void SucessPopup(string message)
        {
            sfPopupLayout = new SfPopupLayout();
            templateView = new DataTemplate(() =>
            {
                popupContent = new Label();
                popupContent.Text = message;
                popupContent.HorizontalTextAlignment = TextAlignment.Center;


                return popupContent;
            });
            sfPopupLayout.PopupView.ShowHeader = false;
            sfPopupLayout.PopupView.ContentTemplate = templateView;
            sfPopupLayout.Show();
        }

        private async void SaveListing(object obj)
        {
            SavedListing savedListing = new SavedListing()
            {
                PropertyId = this.PropertyId,
                MlsNumber = this.MlsNumber,
                Address = this.Address,
                Price = this.Price
            };


            var listings = await Firestore.Read();
            foreach (var listing in listings)
            {

                if (listing.PropertyId == savedListing.PropertyId)
                {
                    await App.Current.MainPage.DisplayAlert("Failed", "Listing has already been saved!", "Ok");
                    return;
                }
            }

            bool result = Consts.Firestore.Insert(savedListing);
            await App.Current.MainPage.DisplayAlert("Success", "Listing saved!", "Ok");
        }
    }
}