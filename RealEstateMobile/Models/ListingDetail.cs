﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RealEstateMobile.Models
{
    public class Building
    {
        public string BathroomTotal { get; set; }
        public string Bedrooms { get; set; }
        public string SizeInterior { get; set; }
        public string StoriesTotal { get; set; }
        public string Type { get; set; }
        public string Ammenities { get; set; }
        public string Appliances { get; set; }
        public string BasementDevelopment { get; set; }
        public string BasementFeatures { get; set; }
        public string BasementType { get; set; }
        public string ConstructedDate { get; set; }
        public string ConstructionStyleAttachment { get; set; }
        public string FireProtection { get; set; }
        public string FireplaceTotal { get; set; }
        public string HeatingFuel { get; set; }
        public string HeatingType { get; set; }
        public string DisplayAsYears { get; set; }
        public string FireplacePresent { get; set; }
        public string BuiltIn { get; set; }
    }

    public class Land
    {
        public string SizeTotal { get; set; }
        public string SizeFrontage { get; set; }
        public bool Acreage { get; set; }
    }




    public class Individual
    {
        public int IndividualID { get; set; }
        public string Name { get; set; }
        public Organization Organization { get; set; }
        public IList<Phone> Phones { get; set; }
        public IList<Website> Websites { get; set; }
        public IList<Email> Emails { get; set; }
        public string Position { get; set; }
        public bool PermitFreetextEmail { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string CorporationDisplayTypeId { get; set; }
        public string RelativeDetailsURL { get; set; }
        public string RankMyAgentKey { get; set; }
        public string RealSatisfiedKey { get; set; }
    }

    //public class Photo
    //{
    //    public string SequenceId { get; set; }
    //    public string HighResPath { get; set; }
    //    public string MedResPath { get; set; }
    //    public string LowResPath { get; set; }
    //    public string LastUpdated { get; set; }
    //}

    public class Parking
    {
        public string Name { get; set; }
        public string Spaces { get; set; }
    }


    public class UploadedByAddress
    {
        public string AddressText { get; set; }
        public bool PermitShowAddress { get; set; }
        public object DisseminationArea { get; set; }
    }


    public class ListingDetail
    {
        public ErrorCode ErrorCode { get; set; }
        public string HashCode { get; set; }
        public string Id { get; set; }
        public string MlsNumber { get; set; }
        public string PublicRemarks { get; set; }
        public string LastUpdated { get; set; }
        public Building Building { get; set; }
        public Land Land { get; set; }
        public IList<Individual> Individual { get; set; }
        public Property Property { get; set; }
        public AlternateURL AlternateURL { get; set; }
        public string UploadedBy { get; set; }
        public Business Business { get; set; }
        public string RelativeURLEn { get; set; }
        public string RelativeURLFr { get; set; }
        public IList<object> History { get; set; }
        public string UploadedByWebsite { get; set; }
        public UploadedByAddress UploadedByAddress { get; set; }
        public IList<Medium> Media { get; set; }
        public string TimeOnRealtor { get; set; }
        public bool IsNewListing { get; set; }
    }


}
