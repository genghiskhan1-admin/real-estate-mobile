﻿using System;
namespace RealEstateMobile
{
    public class HouseListing
    {

        
        public string PropertyId { get; set; }
        public string Address { get; set; }
        public string ImgSrc { get; set; }
        public string MLS { get; set; }
        public string Price { get; set; }

    }
}
