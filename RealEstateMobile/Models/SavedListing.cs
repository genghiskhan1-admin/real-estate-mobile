﻿using System;
namespace RealEstateMobile.Models
{
	public class SavedListing
	{
        public string Id { get; set; }
        public string PropertyId { get; set; }
        public string UserId { get; set; }
        public string MlsNumber { get; set; }
        public string Address { get; set; }
        public string Price { get; set; }

    }
}

