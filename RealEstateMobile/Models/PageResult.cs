﻿using System;
using System.Collections.Generic;

namespace RealEstateMobile.Models
{

    public class PageResult
    {
        public ErrorCode ErrorCode;
        public Paging Paging;
        public Result Results;
        public Pin Pins;
        public string GroupingLevel;
    }

        public class ErrorCode
        {
            public int Id { get; set; }
            public string Description { get; set; }
            public string Status { get; set; }
            public string ProductName { get; set; }
            public string Version { get; set; }
        }

        public class Paging
        {
            public int RecordsPerPage { get; set; }
            public int CurrentPage { get; set; }
            public int TotalRecords { get; set; }
            public int MaxRecords { get; set; }
            public int TotalPages { get; set; }
            public int RecordsShowing { get; set; }
            public int Pins { get; set; }
        }

        public class BuildingListing
        {
            public string StoriesTotal { get; set; }
        }

        public class Address
        {
            public string AddressText { get; set; }
            public bool PermitShowAddress { get; set; }
            public object DisseminationArea { get; set; }
        }

        public class Phone
        {
            public string PhoneType { get; set; }
            public string PhoneNumber { get; set; }
            public string AreaCode { get; set; }
            public string PhoneTypeId { get; set; }
        }

        public class Email
        {
            public string ContactId { get; set; }
        }

        public class Website
        {
            public string Site { get; set; }
            public string WebsiteTypeId { get; set; }
        }

        public class Organization
        {
            public int OrganizationID { get; set; }
            public string Name { get; set; }
            public Address Address { get; set; }
            public IList<Phone> Phones { get; set; }
            public IList<Email> Emails { get; set; }

            public string OrganizationType { get; set; }
            public bool HasEmail { get; set; }
            public bool PermitFreetextEmail { get; set; }
            public bool PermitShowListingLink { get; set; }
            public string RelativeDetailsURL { get; set; }
            public string PhotoLastupdate { get; set; }
            public string Logo { get; set; }
            public IList<Website> Websites { get; set; }
            public string Designation { get; set; }
        }


        public class IndividualListing
        {
            public int IndividualID { get; set; }
            public string Name { get; set; }
            public Organization Organization { get; set; }
            public IList<Phone> Phones { get; set; }
            public IList<Email> Emails { get; set; }
            public bool PermitFreetextEmail { get; set; }
            public string FirstName { get; set; }
            public string LastName { get; set; }
            public bool CccMember { get; set; }
            public string CorporationDisplayTypeId { get; set; }
            public bool PermitShowListingLink { get; set; }
            public string RelativeDetailsURL { get; set; }
            public string RankMyAgentKey { get; set; }
            public string RealSatisfiedKey { get; set; }
            public IList<Website> Websites { get; set; }
            public string Photo { get; set; }
            public string Position { get; set; }
            public string AgentPhotoLastUpdated { get; set; }
            public string PhotoHighRes { get; set; }
            public string CorporationName { get; set; }
            public string CorporationType { get; set; }
        }

        public class Photo
        {
            public string SequenceId { get; set; }
            public string HighResPath { get; set; }
            public string MedResPath { get; set; }
            public string LowResPath { get; set; }
            public string LastUpdated { get; set; }
            public string Description { get; set; }
        }

        public class Property
        {
            public string Price { get; set; }
            public string Type { get; set; }
            public Address Address { get; set; }
            public IList<Photo> Photo { get; set; }
            public string TypeId { get; set; }
            public string ConvertedPrice { get; set; }
            public string PriceUnformattedValue { get; set; }
            public string FarmType { get; set; }
            public string ZoningType { get; set; }

            public string ParkingSpaceTotal { get; set; }
            public string OwnershipType { get; set; }
            public string AmmenitiesNearBy { get; set; }
            public IList<int> OwnershipTypeGroupIds { get; set; }
            public string ParkingType { get; set; }
            public string Features { get; set; }
            public string PoolType { get; set; }
            public string TransactionType { get; set; }
            public string ViewType { get; set; }
            public string TaxAmount { get; set; }
    }

        public class Business
        {
        }

        //public class Land
        //{
        //    public string SizeTotal { get; set; }
        //    public string SizeFrontage { get; set; }
        //}

        public class AlternateURL
        {
            public string VideoLink { get; set; }
            public string DetailsLink { get; set; }
        }

        public class Medium
        {
            public string MediaCategoryId { get; set; }
            public string MediaCategoryURL { get; set; }
            public string Description { get; set; }
            public int Order { get; set; }
        }

        public class Tag
        {
            public string Label { get; set; }
            public string HTMLColorCode { get; set; }
            public string ListingTagTypeID { get; set; }
        }

        public class Result
        {
            public string Id { get; set; }
            public string MlsNumber { get; set; }
            public string PublicRemarks { get; set; }
            public Building Building { get; set; }
            public IList<Individual> Individual { get; set; }
            public Property Property { get; set; }
            public Business Business { get; set; }
            public Land Land { get; set; }
            public AlternateURL AlternateURL { get; set; }
            public string PostalCode { get; set; }
            public string RelativeDetailsURL { get; set; }
            public string StatusId { get; set; }
            public string PhotoChangeDateUTC { get; set; }
            public string Distance { get; set; }
            public string RelativeURLEn { get; set; }
            public string RelativeURLFr { get; set; }
            public IList<Medium> Media { get; set; }
            public string InsertedDateUTC { get; set; }
            public string TimeOnRealtor { get; set; }
            public IList<Tag> Tags { get; set; }
            public string PriceChangeDateUTC { get; set; }
        }

        public class Pin
        {
            public string key { get; set; }
            public string propertyId { get; set; }
            public int count { get; set; }
            public string longitude { get; set; }
            public string latitude { get; set; }
        }

        public class Example
        {
            public ErrorCode ErrorCode { get; set; }
            public Paging Paging { get; set; }
            public IList<Result> Results { get; set; }
            public IList<Pin> Pins { get; set; }
            public string GroupingLevel { get; set; }
        }
}

