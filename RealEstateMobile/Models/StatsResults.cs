﻿using System;
using System.Collections.Generic;
using System.Text;

namespace RealEstateMobile.Models
{
    public class StatsResults
    {

    }
    public class PopByAgeGroup
    {

        public string AgeGroup { get; set; }

        public int AgeGroupValue { get; set; }
    }

    public class PopTrendByYear
    {
        public string Year { get; set; }

        public int PopTrendByYearValue { get; set; }
    }

    public class OwnershipTypeStats
    {
        public string OwnershipType { get; set; }

        public int OwnershipTypeValue { get; set; }
    }

    public class ConstructionDateStats
    {
        public string TimePeriod { get; set; }

        public int TimePeriodValue { get; set; }
    }

    public class MaritalStatusStats
    {
        public string MartialStatus { get; set; }

        public int MartialStatusValue { get; set; }
    }

    public class EducationStats
    {
        public string Education { get; set; }

        public int EducationValue { get; set; }
    }

    public class HouseholdIncomeStats
    {
        public string HouseholdIncome { get; set; }

        public int HouseholdIncomeValue { get; set; }
    }

    public class ChildrenAtHomeStats
    {
        public string ChildrenAtHome { get; set; }

        public int ChildrenAtHomeValue { get; set; }
    }

    public class LanguagesStats
    {
        public string Languages { get; set; }

        public int LanguagesValue { get; set; }
    }

    public class OccupationsStats
    {
        public string Occupations { get; set; }

        public int OccupationsValue { get; set; }
    }
}
