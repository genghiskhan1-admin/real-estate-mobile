﻿using System;
namespace RealEstateMobile.Models
{
    public class RealEstateAgent
    {
        //public RealEstateAgent()
        //{
        //}

        public string RealEstateAgentName { get; set; }
        public int RealEstateAgentAge { get; set; }
        public string RealEstateAgentBrokrage { get; set; }
        public string RealEstateAgentDescription { get; set; }
        public int RealEstateAgentRating { get; set; }

    }
}