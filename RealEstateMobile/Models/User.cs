﻿using System;
using System.Collections.Generic;

namespace RealEstateMobile.Models
{
    public class User
    {
        //public User()
        //{
        //}

        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        //public string PhoneNumber { get; set; } //can change to ulong if you want number to be integer type
        //public string Password { get; set; }
        //public string ConfirmPassword { get; set; }
        //public UserClicks UserClicks { get; set; }
        //public List<HouseListing> FavouritedHouseListings { get; set; }

    }
}
