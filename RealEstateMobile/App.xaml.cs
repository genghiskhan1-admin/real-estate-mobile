﻿using RealEstateMobile.AppLayout;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;


[assembly: ExportFont("Montserrat-Bold.ttf", Alias = "Montserrat-Bold")]
[assembly: ExportFont("Montserrat-Medium.ttf", Alias = "Montserrat-Medium")]
[assembly: ExportFont("Montserrat-Regular.ttf", Alias = "Montserrat-Regular")]
[assembly: ExportFont("Montserrat-SemiBold.ttf", Alias = "Montserrat-SemiBold")]
[assembly: ExportFont("Font Awesome 6 Brands-Regular-400.ttf", Alias = "FAB")]
[assembly: ExportFont("Font Awesome 6 Free-Regular-400.ttf", Alias = "FAR")]
[assembly: ExportFont("Font Awesome 6 Free-Solid-900.ttf", Alias = "FAS")]

namespace RealEstateMobile
{
    public partial class App : Application
    {
        public App()
        {

            Syncfusion.Licensing.SyncfusionLicenseProvider.RegisterLicense("NzU5NTAwQDMyMzAyZTMzMmUzMERoMjRiYjlONE1VM2ZXd2hkQWpRQUFtK3dNTlhxNDkxQ09aN050cklEN3M9");
           
            InitializeComponent();

            OSAppTheme currentTheme = Application.Current.RequestedTheme;

            if (currentTheme == OSAppTheme.Light)
            {
                Application.Current.Resources.ApplyLightTheme();
            }
            else
            {
                Application.Current.Resources.ApplyDarkTheme();
            }

            

            MainPage = new AppShell();
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
