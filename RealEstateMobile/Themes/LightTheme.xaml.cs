﻿using Xamarin.Forms.Internals;
using Xamarin.Forms.Xaml;

namespace RealEstateMobile.Themes
{
    [Preserve(AllMembers = true)]
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LightTheme
    {
        public LightTheme()
        {
            this.InitializeComponent();
        }
    }
}

