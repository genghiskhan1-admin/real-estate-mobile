﻿using System;
using System.Collections.Generic;
using RealEstateMobile.Views;
using Xamarin.Forms;

namespace RealEstateMobile
{
    public partial class AppShell : Shell
    {
        public AppShell()
        {
            InitializeComponent();

            Routing.RegisterRoute(nameof(UserLoginPage), typeof(UserLoginPage));
            Routing.RegisterRoute(nameof(UserProfilePage), typeof(UserProfilePage));
            Routing.RegisterRoute(nameof(UserRegistrationPage), typeof(UserRegistrationPage));
            Routing.RegisterRoute(nameof(UserResetPasswordPage), typeof(UserResetPasswordPage));
            Routing.RegisterRoute(nameof(SavedListings), typeof(SavedListings));
            Routing.RegisterRoute(nameof(ListingDetail), typeof(ListingDetail));
            Routing.RegisterRoute(nameof(ChartsPage), typeof(ChartsPage));
            
        }
    }
}

